package com.food.lending;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.time.Clock;
import java.time.ZoneId;

@EnableConfigurationProperties
@SpringBootApplication
public class FoodLendingApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodLendingApplication.class, args);
	}

	@Bean
	public Clock clock() {
		return Clock.system(ZoneId.of("UTC"));
	}

}
