package com.food.lending.notification.adapter.in.consumer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.food.lending.notification.adapter.in.consumer.exception.RetryableException;
import com.food.lending.notification.adapter.in.consumer.model.OrderEventToProcess;
import com.food.lending.notification.application.exception.HttpErrorException;
import com.food.lending.notification.application.port.in.NotifyOrderCommand;
import com.food.lending.notification.config.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.DltHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderConsumerAdapter {

    private static final String CONTENT = "content";
    private static final String ID = "id";
    private static final String USER_ID = "user_id";
    private static final String STATUS = "status";
    private static final String NAME = "name";

    private final ObjectMapper objectMapper;
    private final NotifyOrderCommand notifyOrderCommand;

    public OrderConsumerAdapter(
            ObjectMapper objectMapper,
            NotifyOrderCommand notifyOrderCommand) {
        this.objectMapper = objectMapper;
        this.notifyOrderCommand = notifyOrderCommand;
    }

    @KafkaListener(
            topics = "${notification.events.order-topic}",
            groupId = "${notification.events.group-id}"
    )
    public void consume(@Payload Message<String> message, Acknowledgment ack) {
        JsonNode event = deserialize(message.getPayload(), ack);
        String eventName = event.get(NAME).asText();

        if (OrderEventToProcess.discardEvent(eventName)) {
            ack.acknowledge();
            return;
        }

        log.info("start process event with name: {} and payload: {}", eventName, message);

        try {
            OrderEventToProcess orderEventToProcess = OrderEventToProcess.getFromName(eventName);

            NotifyOrderCommand.Data.DataBuilder data = NotifyOrderCommand.Data.builder()
                    .orderId(event.get(CONTENT).get(ID).asInt())
                    .userId(event.get(CONTENT).get(USER_ID).asInt())
                    .orderStatus(event.get(CONTENT).get(STATUS).asText());

            switch (orderEventToProcess) {
                case ORDER_CREATED:
                    notifyOrderCommand.execute(data.build());
                    break;
            }
        } catch (HttpErrorException e) {
            if (ErrorCode.REPOSITORY_NOT_AVAILABLE.equals(e.getErrorResponse().getCode())) {
                throw new RetryableException(e);
            }
            log.error("Error processing order event: {}, error response: {}", eventName, e.getErrorResponse());
        } catch (Exception e) {
            log.error("Error processing order event: {}, error message: {}", eventName, e);
        }

        ack.acknowledge();
        log.info("event processed successfully");
    }

    @DltHandler
    public void handleDtlMessage(String payload, Acknowledgment ack) {
        log.info("dtl topic: Error processing order event with payload: {}", payload);
        ack.acknowledge();
    }

    private JsonNode deserialize(String payload, Acknowledgment ack) {
        try {
            return objectMapper.readValue(payload, JsonNode.class);
        } catch (Exception e) {
            ack.acknowledge();
            log.error("Unable to deserialize payload: {}", payload);
            throw new RuntimeException();
        }
    }
}
