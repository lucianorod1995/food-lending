package com.food.lending.notification.adapter.in.consumer.exception;

import com.food.lending.notification.application.exception.HttpErrorException;

public class RetryableException extends RuntimeException {
    private final Exception originalException;

    public RetryableException(HttpErrorException originalException) {
        this.originalException = originalException;
    }

    @Override
    public String getMessage() {
        return this.originalException.getMessage();
    }
}
