package com.food.lending.notification.adapter.in.consumer.model;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Event {
    Object content;
    String idempotencyKey;
    String name;
}
