package com.food.lending.notification.adapter.in.consumer.model;

import com.food.lending.notification.application.exception.EntityBadRequestException;
import com.food.lending.notification.config.ErrorCode;

import java.util.Arrays;

public enum OrderEventToProcess {
    ORDER_CREATED;

    public static OrderEventToProcess getFromName(String name) {
        return Arrays.stream(OrderEventToProcess.values())
                .filter(connector -> connector.name().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow( () -> {
                    throw new EntityBadRequestException(ErrorCode.INVALID_EVENT_NAME);
                });
    }

    public static Boolean discardEvent(String name) {
        return Arrays.stream(OrderEventToProcess.values())
                .noneMatch(event -> event.name().equalsIgnoreCase(name));
    }
}
