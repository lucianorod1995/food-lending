package com.food.lending.notification.application.exception;

import com.food.lending.notification.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class BusinessException extends HttpErrorException {

    public BusinessException(ErrorCode errorCode) {
        super(HttpStatus.BAD_REQUEST.value(), errorCode);
    }
}
