package com.food.lending.notification.application.exception;

import com.food.lending.notification.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityConflictException extends HttpErrorException {

    public EntityConflictException(ErrorCode errorCode) {
        super(HttpStatus.CONFLICT.value(), errorCode);
    }
}
