package com.food.lending.notification.application.exception;

import com.food.lending.notification.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityForbiddenException extends HttpErrorException {

    public EntityForbiddenException(ErrorCode errorCode) {
        super(HttpStatus.FORBIDDEN.value(), errorCode);
    }
}
