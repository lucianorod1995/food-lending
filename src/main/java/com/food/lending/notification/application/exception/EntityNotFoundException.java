package com.food.lending.notification.application.exception;

import com.food.lending.notification.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityNotFoundException extends HttpErrorException {

    public EntityNotFoundException(ErrorCode errorCode) {
        super(HttpStatus.NOT_FOUND.value(), errorCode);
    }
}
