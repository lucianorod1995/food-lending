package com.food.lending.notification.application.exception;

import com.food.lending.notification.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityUnauthorizedException extends HttpErrorException {

    public EntityUnauthorizedException(ErrorCode errorCode) {
        super(HttpStatus.UNAUTHORIZED.value(), errorCode);
    }
}
