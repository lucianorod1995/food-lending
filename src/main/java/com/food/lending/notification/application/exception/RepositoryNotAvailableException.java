package com.food.lending.notification.application.exception;

import com.food.lending.notification.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class RepositoryNotAvailableException extends HttpErrorException {

    public RepositoryNotAvailableException(ErrorCode errorCode) {
        super(HttpStatus.SERVICE_UNAVAILABLE.value(), errorCode);
    }
}
