package com.food.lending.notification.application.port.in;

import lombok.Builder;
import lombok.Value;

public interface NotifyOrderCommand {

    void execute(Data data);

    @Builder
    @Value
    class Data {
        Integer userId;
        Integer orderId;
        String orderStatus;
    }
}
