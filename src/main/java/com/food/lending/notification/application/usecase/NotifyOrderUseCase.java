package com.food.lending.notification.application.usecase;

import com.food.lending.notification.application.port.in.NotifyOrderCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotifyOrderUseCase implements NotifyOrderCommand {

    public NotifyOrderUseCase() {
    }

    @Override
    public void execute(Data data) {
        log.info("Notifying order {}", data);
    }
}
