package com.food.lending.notification.config;

public enum ErrorCode {

    MANDATORY_ARGUMENT_NOT_FOUND(Constants.MANDATORY_ARGUMENT_NOT_FOUND),
    INVALID_ARGUMENT(Constants.INVALID_ARGUMENT),
    REPOSITORY_NOT_AVAILABLE(Constants.REPOSITORY_NOT_AVAILABLE),
    INVALID_EVENT_NAME(Constants.INVALID_EVENT_NAME);

    ErrorCode(String value) {
    }

    public static class Constants {
        public static final String REPOSITORY_NOT_AVAILABLE = "REPOSITORY_NOT_AVAILABLE";
        public static final String MANDATORY_ARGUMENT_NOT_FOUND = "MANDATORY_ARGUMENT_NOT_FOUND";
        public static final String INVALID_ARGUMENT = "INVALID_ARGUMENT";
        public static final String INVALID_EVENT_NAME = "INVALID_EVENT_NAME";
    }
}
