package com.food.lending.order.adapter.in.controller;

import com.food.lending.order.adapter.in.controller.model.request.CreateOrderRequestBody;
import com.food.lending.order.adapter.in.controller.model.response.OrderRestModel;
import com.food.lending.order.application.port.in.CreateOrderCommand;
import com.food.lending.order.domain.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class OrderController {

    private static final String PATH_ORDERS = "/orders";

    private final CreateOrderCommand createOrderCommand;

    public OrderController(CreateOrderCommand createOrderCommand) {
        this.createOrderCommand = createOrderCommand;
    }

    @PostMapping(PATH_ORDERS)
    @ResponseStatus(HttpStatus.CREATED)
    public OrderRestModel order(@RequestBody CreateOrderRequestBody request) {
        log.info("Received call to POST {}", PATH_ORDERS);
        CreateOrderCommand.Data data = CreateOrderCommand.Data
                .builder()
                .userId(request.getUserId())
                .foodIds(request.getFoodIds())
                .build();
        Order order = createOrderCommand.execute(data);
        OrderRestModel response = OrderRestModel.fromDomain(order);
        log.info("Order transaction: {}", response);
        return response;
    }

}
