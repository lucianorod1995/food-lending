package com.food.lending.order.adapter.in.controller.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.food.lending.order.config.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CreateOrderRequestBody {
    @NotNull(message = ErrorCode.Constants.MANDATORY_ARGUMENT_NOT_FOUND)
    private Integer userId;
    @NotEmpty(message = ErrorCode.Constants.MANDATORY_ARGUMENT_NOT_FOUND)
    private List<Integer> foodIds;
}
