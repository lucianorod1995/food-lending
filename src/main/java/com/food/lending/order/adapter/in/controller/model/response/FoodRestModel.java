package com.food.lending.order.adapter.in.controller.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.food.lending.order.domain.Food;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class FoodRestModel {
    private Integer id;
    private BigDecimal price;

    public static FoodRestModel fromDomain(Food food) {
        return FoodRestModel.builder()
                .id(food.getId())
                .price(food.getPrice())
                .build();
    }
}
