package com.food.lending.order.adapter.in.controller.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.food.lending.order.domain.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class OrderRestModel {

    private Integer id;
    private Integer userId;
    private List<FoodRestModel> foods;
    private BigDecimal totalPrice;

    public static OrderRestModel fromDomain(Order order) {
        List<FoodRestModel> foodRestModels = order.getFoods().stream()
                .map(FoodRestModel::fromDomain)
                .collect(Collectors.toList());
        return OrderRestModel.builder()
                .id(order.getId())
                .userId(order.getUserId())
                .foods(foodRestModels)
                .totalPrice(order.getTotalPrice())
                .build();
    }
}
