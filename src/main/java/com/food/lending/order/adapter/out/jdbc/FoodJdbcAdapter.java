package com.food.lending.order.adapter.out.jdbc;

import com.food.lending.order.domain.Food;
import com.food.lending.order.utils.NumberUtils;
import com.food.lending.order.application.port.out.FoodRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class FoodJdbcAdapter implements FoodRepository {

    public FoodJdbcAdapter() {
    }

    @Override
    public List<Food> findAllById(List<Integer> foodIds) {
        List<Food> foods = foodIds.stream()
                .map(
                    id -> Food.builder()
                            .id(id)
                            .price(NumberUtils.random(10000L))
                            .build()
                )
                .collect(Collectors.toList());
        log.info("Foods found!");
        return foods;
    }

}
