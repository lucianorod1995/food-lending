package com.food.lending.order.adapter.out.jdbc;

import com.food.lending.order.application.port.out.OrderRepository;
import com.food.lending.order.domain.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderJdbcAdapter implements OrderRepository {

    public OrderJdbcAdapter() {
    }

    @Override
    public Order save(Order order) {
        log.info("Order saved!");
        return order.withId((int) (Math.random()*10000));
    }

}
