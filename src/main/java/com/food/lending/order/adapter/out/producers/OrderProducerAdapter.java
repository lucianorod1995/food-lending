package com.food.lending.order.adapter.out.producers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.food.lending.order.adapter.out.producers.model.Event;
import com.food.lending.order.adapter.out.producers.model.OrderEventModel;
import com.food.lending.order.application.port.out.OrderRepository;
import com.food.lending.order.adapter.out.producers.model.OrderEventsConfigurationProperties;
import com.food.lending.order.domain.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.UUID;

@Component
@Slf4j
public class OrderProducerAdapter implements OrderRepository {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;
    private final OrderEventsConfigurationProperties orderEventsConfigurationProperties;

    public OrderProducerAdapter(KafkaTemplate<String, String> kafkaTemplate,
                                ObjectMapper objectMapper,
                                OrderEventsConfigurationProperties orderEventsConfigurationProperties) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
        this.orderEventsConfigurationProperties = orderEventsConfigurationProperties;
    }

    @Override
    public void notify(Order order) {
        String orderStatusKey = order.getStatus().name().toLowerCase();
        String eventName = orderEventsConfigurationProperties.getNames().get(orderStatusKey);
        if (!StringUtils.hasText(eventName))
            return;

        Event<OrderEventModel> event = Event.<OrderEventModel>builder()
                .content(OrderEventModel.fromDomain(order))
                .idempotencyKey(generateIdempotencyKey())
                .name(eventName)
                .build();

        sendEvent(event, order.getId().toString());
    }

    private <T> void sendEvent(Event<T> event, String key) {
        log.info("sending event to topic: {} with idempotency-key: {}", orderEventsConfigurationProperties.getTopic(), event.getIdempotencyKey());
        log.debug("event payload: {}", event);
        try {
            ProducerRecord<String, String> record = new ProducerRecord<>(orderEventsConfigurationProperties.getTopic(), key, objectMapper.writeValueAsString(event));
            kafkaTemplate.send(record).addCallback(
                    success -> log.info("event {} sent successfully", event.getName()),
                    ex -> log.error("error sending notification to topic: {} with payload: {}, exception message: {}", orderEventsConfigurationProperties.getTopic(), event, ex.getMessage())
            );
        } catch (Exception ex) {
            log.error("error sending notification to topic: {} with payload: {}, exception message: {}", orderEventsConfigurationProperties.getTopic(), event, ex.getMessage());
        }
    }

    public String generateIdempotencyKey() {
        return UUID.randomUUID().toString();
    }
}
