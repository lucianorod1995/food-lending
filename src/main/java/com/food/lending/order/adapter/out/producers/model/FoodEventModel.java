package com.food.lending.order.adapter.out.producers.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.food.lending.order.domain.Food;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
public class FoodEventModel {
    Integer id;
    BigDecimal price;

    public static FoodEventModel fromDomain(Food food) {
        return FoodEventModel.builder()
                .id(food.getId())
                .price(food.getPrice())
                .build();
    }
}
