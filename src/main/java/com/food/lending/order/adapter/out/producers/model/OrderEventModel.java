package com.food.lending.order.adapter.out.producers.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.food.lending.order.domain.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
public class OrderEventModel {
    Integer id;
    Integer userId;
    List<FoodEventModel> foods;
    BigDecimal totalPrice;
    String status;

    public static OrderEventModel fromDomain(Order order) {
        List<FoodEventModel> foodEventModels = order.getFoods().stream()
                .map(FoodEventModel::fromDomain)
                .collect(Collectors.toList());
        return OrderEventModel.builder()
                .id(order.getId())
                .userId(order.getUserId())
                .foods(foodEventModels)
                .totalPrice(order.getTotalPrice())
                .status(order.getStatus().name())
                .build();
    }
}
