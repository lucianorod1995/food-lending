package com.food.lending.order.application.exception;

import com.food.lending.order.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class BusinessException extends HttpErrorException {

    public BusinessException(ErrorCode errorCode) {
        super(HttpStatus.BAD_REQUEST.value(), errorCode);
    }
}
