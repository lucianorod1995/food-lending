package com.food.lending.order.application.exception;

import com.food.lending.order.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityBadRequestException extends HttpErrorException{
    public EntityBadRequestException(ErrorCode errorCode) {
        super(HttpStatus.BAD_REQUEST.value(), errorCode);
    }
}
