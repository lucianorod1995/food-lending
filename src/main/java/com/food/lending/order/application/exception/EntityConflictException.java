package com.food.lending.order.application.exception;

import com.food.lending.order.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityConflictException extends HttpErrorException {

    public EntityConflictException(ErrorCode errorCode) {
        super(HttpStatus.CONFLICT.value(), errorCode);
    }
}
