package com.food.lending.order.application.exception;

import com.food.lending.order.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityForbiddenException extends HttpErrorException {

    public EntityForbiddenException(ErrorCode errorCode) {
        super(HttpStatus.FORBIDDEN.value(), errorCode);
    }
}
