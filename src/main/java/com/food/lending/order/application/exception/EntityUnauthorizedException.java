package com.food.lending.order.application.exception;

import com.food.lending.order.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityUnauthorizedException extends HttpErrorException {

    public EntityUnauthorizedException(ErrorCode errorCode) {
        super(HttpStatus.UNAUTHORIZED.value(), errorCode);
    }
}
