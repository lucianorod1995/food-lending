package com.food.lending.order.application.exception;

public class RepositoryHttpErrorException extends HttpErrorException {

    private final String responseBody;
    private final HttpErrorException cause;

    public RepositoryHttpErrorException (HttpErrorException cause, String responseBody) {
        super(cause.getErrorResponse().getHttpStatus(), cause.getErrorResponse().getCode(), cause.getErrorResponse().getFields());
        this.responseBody = responseBody;
        this.cause = cause;
    }

    public String getResponseBody() {
        return responseBody;
    }

    @Override
    public HttpErrorException getCause() {
        return cause;
    }
}
