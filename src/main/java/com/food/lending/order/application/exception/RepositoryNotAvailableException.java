package com.food.lending.order.application.exception;

import com.food.lending.order.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class RepositoryNotAvailableException extends HttpErrorException {

    public RepositoryNotAvailableException(ErrorCode errorCode) {
        super(HttpStatus.SERVICE_UNAVAILABLE.value(), errorCode);
    }
}
