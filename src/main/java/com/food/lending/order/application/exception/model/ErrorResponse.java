package com.food.lending.order.application.exception.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.food.lending.order.config.ErrorCode;
import lombok.Builder;
import lombok.Value;

import java.util.Map;


/*
Example of response:
{
    "code": "USER_CONFLICT",
    "fields": [                  (OPTIONAL FIELD, if not necessary put empty map)
        "name": "NAME_TOO_LONG",
        "password": "INVALID_PASSWORD"
    ]
}
 */
@Value
@Builder
public class ErrorResponse {
	@JsonIgnore
	int httpStatus;
	ErrorCode code;
	Map<String, String> fields;
}
