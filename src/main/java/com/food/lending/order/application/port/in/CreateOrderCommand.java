package com.food.lending.order.application.port.in;

import com.food.lending.order.domain.Order;
import lombok.Builder;
import lombok.Value;

import java.util.List;

public interface CreateOrderCommand {

    Order execute(Data data);

    @Builder
    @Value
    class Data{
         Integer userId;
         List<Integer> foodIds;
    }
}
