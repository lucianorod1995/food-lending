package com.food.lending.order.application.port.out;

import com.food.lending.order.domain.Food;

import java.util.List;

public interface FoodRepository {

    default List<Food> findAllById(List<Integer> foodIds) {
        throw new UnsupportedOperationException();
    }
}
