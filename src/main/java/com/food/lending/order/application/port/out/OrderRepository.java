package com.food.lending.order.application.port.out;

import com.food.lending.order.domain.Order;

public interface OrderRepository {

    default Order save(Order order) {
        throw new UnsupportedOperationException();
    }

    default void notify(Order order) {
        throw new UnsupportedOperationException();
    }
}
