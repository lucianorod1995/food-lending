package com.food.lending.order.application.usecase;

import com.food.lending.order.application.port.in.CreateOrderCommand;
import com.food.lending.order.application.port.out.OrderRepository;
import com.food.lending.order.application.port.out.FoodRepository;
import com.food.lending.order.domain.Food;
import com.food.lending.order.domain.Order;
import com.food.lending.order.domain.OrderStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class CreateOrderUseCase implements CreateOrderCommand {

    private final FoodRepository foodRepository;
    private final OrderRepository orderJdbcAdapter;
    private final OrderRepository orderProducerAdapter;

    public CreateOrderUseCase(
            FoodRepository foodRepository,
            OrderRepository orderJdbcAdapter,
            OrderRepository orderProducerAdapter) {
        this.foodRepository = foodRepository;
        this.orderJdbcAdapter = orderJdbcAdapter;
        this.orderProducerAdapter = orderProducerAdapter;
    }

    @Override
    public Order execute(Data data) {
        List<Food> foods = foodRepository.findAllById(data.getFoodIds());
        Order order = Order.builder()
                .foods(foods)
                .userId(data.getUserId())
                .status(OrderStatus.CREATED)
                .build();

        order = orderJdbcAdapter.save(order);
        orderProducerAdapter.notify(order);
        return order;
    }
}
