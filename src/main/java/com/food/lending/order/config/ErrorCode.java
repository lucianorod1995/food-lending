package com.food.lending.order.config;

public enum ErrorCode {

    MANDATORY_ARGUMENT_NOT_FOUND(Constants.MANDATORY_ARGUMENT_NOT_FOUND),
    INVALID_ARGUMENT(Constants.INVALID_ARGUMENT);

    ErrorCode(String value) {
    }

    public static class Constants {
        public static final String MANDATORY_ARGUMENT_NOT_FOUND = "MANDATORY_ARGUMENT_NOT_FOUND";
        public static final String INVALID_ARGUMENT = "INVALID_ARGUMENT";
    }
}
