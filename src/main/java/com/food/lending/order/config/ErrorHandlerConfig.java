package com.food.lending.order.config;

import com.food.lending.order.application.exception.EntityForbiddenException;
import com.food.lending.order.application.exception.EntityUnauthorizedException;
import com.food.lending.order.application.exception.RepositoryBusinessException;
import com.food.lending.order.application.exception.RepositoryHttpErrorException;
import com.food.lending.order.application.exception.BusinessException;
import com.food.lending.order.application.exception.EntityConflictException;
import com.food.lending.order.application.exception.EntityNotFoundException;
import com.food.lending.order.application.exception.RepositoryNotAvailableException;
import com.food.lending.order.application.exception.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ErrorHandlerConfig {

    private static String camelToSnake(String str) {
        return str.replaceAll("([a-z])([A-Z]+)", "$1_$2").toLowerCase();
    }

    @ExceptionHandler({EntityUnauthorizedException.class})
    public ResponseEntity<ErrorResponse> handle(EntityUnauthorizedException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({BusinessException.class})
    public ResponseEntity<ErrorResponse> handle(BusinessException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({RepositoryHttpErrorException.class})
    public ResponseEntity<ErrorResponse> handle(RepositoryHttpErrorException ex) {
        log.error("Error code handled : {}", ex.getErrorResponse().getCode());
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({EntityForbiddenException.class})
    public ResponseEntity<ErrorResponse> handle(EntityForbiddenException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<ErrorResponse> handle(EntityNotFoundException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({EntityConflictException.class})
    public ResponseEntity<ErrorResponse> handle(EntityConflictException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({RepositoryNotAvailableException.class})
    public ResponseEntity<ErrorResponse> handle(RepositoryNotAvailableException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({RepositoryBusinessException.class})
    public ResponseEntity<ErrorResponse> handle(RepositoryBusinessException ex) {
        log.error("Error handled : ", ex);
        return ResponseEntity.status(ex.getErrorResponse().getHttpStatus()).body(ex.getErrorResponse());
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<ErrorResponse> handle(IllegalArgumentException ex) {
        log.error("Bad argument", ex);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ErrorResponse.builder().code(ErrorCode.INVALID_ARGUMENT).build());
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<ErrorResponse> handle(MethodArgumentTypeMismatchException ex) {
        log.error("Bad argument", ex);
        Map<String, String> invalidArguments = new HashMap<>();

        invalidArguments.put(ex.getParameter().getParameterName(), String.valueOf(ex.getValue()));
        ErrorCode errorCode = ErrorCode.INVALID_ARGUMENT;

        ErrorResponse errorResponse = getInvalidArgumentError(errorCode, invalidArguments);

        return ResponseEntity.status(errorResponse.getHttpStatus()).body(errorResponse);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ErrorResponse> handle(MethodArgumentNotValidException ex) {
        log.error("Bad argument", ex);
        Map<String, String> invalidArguments = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            invalidArguments.put(camelToSnake(fieldName), errorMessage);
        });

        ErrorCode errorCode = ErrorCode.INVALID_ARGUMENT;
        ErrorResponse errorResponse = getInvalidArgumentError(errorCode, invalidArguments);
        return ResponseEntity.status(errorResponse.getHttpStatus()).body(errorResponse);
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    public ResponseEntity<ErrorResponse> handle(MissingServletRequestParameterException ex) {
        log.error("Bad argument", ex);
        Map<String, String> invalidArguments = new HashMap<>();
        invalidArguments.put(ex.getParameterName(), String.valueOf(ex.getMessage()));
        ErrorResponse errorResponse = getInvalidArgumentError(ErrorCode.MANDATORY_ARGUMENT_NOT_FOUND, invalidArguments);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }

    @ExceptionHandler({Throwable.class})
    public ResponseEntity handle(Throwable ex) {
        log.error("Unexpected Error", ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    private ErrorResponse getInvalidArgumentError(ErrorCode errorCode, Map<String, String> fields) {
        ErrorResponse errorResponse =
                ErrorResponse.builder()
                        .code(errorCode)
                        .fields(fields)
                        .httpStatus(HttpStatus.BAD_REQUEST.value())
                        .build();

        log.error("{} : {}", errorCode.name(), fields);

        return errorResponse;
    }
}
