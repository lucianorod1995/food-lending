package com.food.lending.order.config;

import com.food.lending.notification.adapter.in.consumer.exception.RetryableException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.retrytopic.RetryTopicConfiguration;
import org.springframework.kafka.retrytopic.RetryTopicConfigurationBuilder;

@Configuration
public class KafkaConfig {

    @Value("${food.lending.kafka.consumer.retry.initial-interval}")
    private Long initialInterval;

    @Value("${food.lending.kafka.consumer.retry.multiplier}")
    private Double multiplier;

    @Value("${food.lending.kafka.consumer.retry.max-interval}")
    private Long maxInterval;

    @Value("${food.lending.kafka.consumer.retry.max-attempts}")
    private Integer maxAttempts;

    @Value("${food.lending.kafka.consumer.retry.timeout}")
    private Integer timeout;

    @Bean
    public RetryTopicConfiguration retryTopic(KafkaTemplate<String, String> kafkaTemplate) {
        return RetryTopicConfigurationBuilder
                .newInstance()
                .exponentialBackoff(initialInterval, multiplier, maxInterval)
                .maxAttempts(maxAttempts)
                .retryOn(RetryableException.class)
                .timeoutAfter(timeout)
                .create(kafkaTemplate);
    }
}
