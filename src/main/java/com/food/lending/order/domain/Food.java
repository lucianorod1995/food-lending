package com.food.lending.order.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Value;
import lombok.With;

import java.math.BigDecimal;

@Value
@Data
@Builder
public class Food {
    @With
    Integer id;
    BigDecimal price;
}
