package com.food.lending.order.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Value;
import lombok.With;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;


@Value
@Data
@Builder
public class Order {
    @With
    Integer id;
    Integer userId;
    List<Food> foods;
    OrderStatus status;

    public BigDecimal getTotalPrice() {
        if (CollectionUtils.isEmpty(foods)) return BigDecimal.ZERO;
        return foods.stream()
                .map(food -> food.getPrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
