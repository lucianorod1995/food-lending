package com.food.lending.order.domain;

public enum OrderStatus {
    CREATED,
    CANCELLED
}
