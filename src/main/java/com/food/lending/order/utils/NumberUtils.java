package com.food.lending.order.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

    public static BigDecimal random(Long max) {
        BigDecimal maxNumber = new BigDecimal(max);
        BigDecimal random = BigDecimal.valueOf(Math.random());
        return random.multiply(maxNumber).setScale(2, RoundingMode.HALF_UP);
    }
}
