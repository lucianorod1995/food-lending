package com.food.lending.transaction.adapter.out.jdbc;

import com.food.lending.transaction.application.port.out.TransactionRepository;
import com.food.lending.transaction.domain.Transaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TransactionJdbcAdapter implements TransactionRepository {

    public TransactionJdbcAdapter() {
    }

    @Override
    public Transaction save(Transaction transaction) {
        log.info("Transaction saved!");
        return transaction.withId((int) (Math.random() * 10000));
    }

}
