package com.food.lending.transaction.application.exception;

import com.food.lending.transaction.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityConflictException extends HttpErrorException {

    public EntityConflictException(ErrorCode errorCode) {
        super(HttpStatus.CONFLICT.value(), errorCode);
    }
}
