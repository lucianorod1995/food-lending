package com.food.lending.transaction.application.exception;

import com.food.lending.transaction.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityForbiddenException extends HttpErrorException {

    public EntityForbiddenException(ErrorCode errorCode) {
        super(HttpStatus.FORBIDDEN.value(), errorCode);
    }
}
