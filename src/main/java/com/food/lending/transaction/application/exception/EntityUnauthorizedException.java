package com.food.lending.transaction.application.exception;

import com.food.lending.transaction.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class EntityUnauthorizedException extends HttpErrorException {

    public EntityUnauthorizedException(ErrorCode errorCode) {
        super(HttpStatus.UNAUTHORIZED.value(), errorCode);
    }
}
