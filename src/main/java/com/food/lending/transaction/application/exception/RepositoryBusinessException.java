package com.food.lending.transaction.application.exception;

import com.food.lending.transaction.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class RepositoryBusinessException extends HttpErrorException {

    public RepositoryBusinessException(ErrorCode errorCode) {
        super(HttpStatus.BAD_REQUEST.value(), errorCode);
    }
}
