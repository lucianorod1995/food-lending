package com.food.lending.transaction.application.exception;

import com.food.lending.transaction.config.ErrorCode;
import org.springframework.http.HttpStatus;

public class RepositoryNotAvailableException extends HttpErrorException {

    public RepositoryNotAvailableException(ErrorCode errorCode) {
        super(HttpStatus.SERVICE_UNAVAILABLE.value(), errorCode);
    }
}
