package com.food.lending.transaction.application.port.in;

import com.food.lending.transaction.domain.Transaction;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

public interface CreateTransactionCommand {

    Transaction execute(Data data);

    @Builder
    @Value
    class Data{
         Integer userId;
         BigDecimal amount;
         String type;
    }
}
