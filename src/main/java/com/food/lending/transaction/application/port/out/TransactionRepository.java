package com.food.lending.transaction.application.port.out;

import com.food.lending.transaction.domain.Transaction;

public interface TransactionRepository {
    default Transaction save(Transaction order) {
        throw new UnsupportedOperationException();
    }
}
