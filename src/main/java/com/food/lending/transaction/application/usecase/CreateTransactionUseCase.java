package com.food.lending.transaction.application.usecase;

import com.food.lending.transaction.application.port.in.CreateTransactionCommand;
import com.food.lending.transaction.application.port.out.TransactionRepository;
import com.food.lending.transaction.domain.Transaction;
import com.food.lending.transaction.domain.TransactionType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CreateTransactionUseCase implements CreateTransactionCommand {

    private final TransactionRepository transactionJdbcAdapter;

    public CreateTransactionUseCase(TransactionRepository transactionJdbcAdapter) {
        this.transactionJdbcAdapter = transactionJdbcAdapter;
    }

    @Override
    public Transaction execute(Data data) {
        Transaction transaction = Transaction.builder()
                .type(TransactionType.valueOf(data.getType().toUpperCase()))
                .amount(data.getAmount())
                .userId(data.getUserId())
                .build();

        transaction = transactionJdbcAdapter.save(transaction);
        return transaction;
    }
}
