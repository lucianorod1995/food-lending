package com.food.lending.transaction.domain;

import lombok.Builder;
import lombok.Data;
import lombok.Value;
import lombok.With;

import java.math.BigDecimal;

@Value
@Data
@Builder
public class Transaction {
    @With
    Integer id;
    Integer userId;
    BigDecimal amount;
    TransactionType type;
}
