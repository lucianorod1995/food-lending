package com.food.lending.transaction.domain;

public enum TransactionType {
    DEBT,
    PAYMENT
}
